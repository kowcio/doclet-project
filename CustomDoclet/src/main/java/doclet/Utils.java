package doclet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created utils for making life easier.
 */
public class Utils {
    
    /**
     * For tests
     * 
     * @param camelCaseString
     * @return
     */
/*    public static void main(String[] args) {
        
        System.out.println(getCamelCaseToNormalForm("checkSearchPageForDoctorAndDepartment"));
        
    }*/
    
    public static String getCamelCaseToNormalForm(String camelCaseString) {
        
        String[] string = camelCaseString.split("(?=\\p{Upper})");
        List<String> normalMethodName = new ArrayList<String>();
        Arrays.asList(string).forEach(s -> {
            s = s.toLowerCase();
            normalMethodName.add(s);
        });
        String first = normalMethodName.get(0);
        normalMethodName.set(0, capitalize(first));
        return join(normalMethodName, " ");
        
    }
    
    private static String capitalize(String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }
    
    private static String join(List<String> list, String delim) {
        
        StringBuilder sb = new StringBuilder();
        
        String loopDelim = "";
        
        for (String s : list) {
            
            sb.append(loopDelim);
            sb.append(s);
            
            loopDelim = delim;
        }
        
        return sb.toString();
    }
}
